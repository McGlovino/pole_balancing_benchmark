﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class Functions
{
    public static float RangeChange(float OldValue, float OldMin, float OldMax, float NewMin, float NewMax)
    {
        float NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin;
        return NewValue;
    }
    public static Byte[] SerializeToBytes(object o)
    {
        MemoryStream stream = new MemoryStream();
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, o);
        return stream.ToArray();
    }
    public static object DeserializeFromBytes(Byte[] bytes)
    {
        MemoryStream stream = new MemoryStream(bytes);
        BinaryFormatter formatter = new BinaryFormatter();
        stream.Seek(0, SeekOrigin.Begin);
        object o = formatter.Deserialize(stream);
        return o;
    }

    public static Byte[] Crossover(byte[] a, byte[] b)
    {
        int splitPoint = UnityEngine.Random.Range(0, a.Length);

        byte[] rv = new byte[a.Length];
        a.CopyTo(rv, 0);

        for (int i = splitPoint; i < a.Length; i++)
            rv[i] = b[i];
        return rv;
    }
}
