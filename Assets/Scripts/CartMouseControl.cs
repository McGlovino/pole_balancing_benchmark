﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartMouseControl : MonoBehaviour
{
    Rigidbody2D rigid;

    Vector3 mousePos;
    Vector2 direction;

    float moveSpeed = 50f;

    public float input = 0;

    void Start()
    {
        rigid = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.x = Mathf.Clamp(mousePos.x, -SystemVariables.Instance.railHalfLength, SystemVariables.Instance.railHalfLength);
        mousePos.y = transform.position.y;
        direction = (mousePos - transform.position).normalized;
        //Debug.Log(direction.magnitude);
        //if (direction.magnitude != 0)
            //direction.Normalize();

        rigid.velocity = new Vector2(direction.x * moveSpeed, 0);
    }
}
