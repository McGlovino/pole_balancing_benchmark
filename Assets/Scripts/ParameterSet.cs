﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParameterSet
{
    public int testingFor;

    public List<float> genCompleted;

    public int maxMutations;
    public int mutationRange;
    public float mutationChance;
    public int noSimulated;
    public float percentileBred;
    public ParameterSet(int tf, int mm, int mr, float mc, int ns, float pb)
    {
        genCompleted = new List<float>();

        testingFor = tf;
        maxMutations = mm;
        mutationRange = mr;
        mutationChance = mc;
        noSimulated = ns;
        percentileBred = pb;
    }

    public float AverageGenCompletion()
    {
        float rv = 0;
        foreach (int i in genCompleted)
            rv += i;
        return rv / genCompleted.Count;
    }

    public void Print()
    {
        string toPrint = "";
        toPrint += "mm: " + maxMutations;
        toPrint += "\nmr: " + mutationRange;
        toPrint += "\nmc: " + mutationChance;
        toPrint += "\nns: " + noSimulated;
        toPrint += "\npb: " + percentileBred;

        Debug.Log(toPrint);
    }
}
