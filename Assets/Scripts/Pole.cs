﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class Pole : MonoBehaviour
{
    public bool doublePole;

    Rigidbody2D rb;
    Vector3 startPos;

    Color32 red = new Color32(210, 125, 125, 255);
    Color32 blue = new Color32(125, 175, 210, 255);

    public SystemVariables SV;

    void Start()
    {
        startPos = transform.localPosition;
        rb = GetComponent<Rigidbody2D>();

        //variance at start so conditions not always the same / cant learn to not move
        rb.angularVelocity = Random.Range(-SV.poleInitialVelocityRange, SV.poleInitialVelocityRange);
    }
    void Update()
    {
        //a clamped range is needed to convert it into -1 to 1 range
        rb.angularVelocity = Mathf.Clamp(rb.angularVelocity, -SV.poleVelocityClamp, SV.poleVelocityClamp);
    }

    public void Reset()
    {
        gameObject.GetComponent<SpriteShapeRenderer>().color = red;
        transform.localPosition = new Vector3(0, 2, 0);
        transform.rotation = Quaternion.identity;
        rb.angularVelocity = Random.Range(-SV.poleInitialVelocityRange, SV.poleInitialVelocityRange);
    }

    public void SetBlue()
    {
        gameObject.GetComponent<SpriteShapeRenderer>().color = blue;
    }
}
