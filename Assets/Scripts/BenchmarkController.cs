﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BenchmarkController : MonoBehaviour
{
    public GameObject PCRObj;
    public Text generationText;
    public Text mutParameterText;
    public Text sysParameterText;
    public Text exitConditionText;
    public Text setText;
    public Text optimumText;

    int generationCount = 1;
    float counter = 0;

    List<CartBrainControl> carts;
    List<float> scores;
    List<ParameterSet> paramSets;
    public ParameterSet currParamSet;
    int currentParamSetNo = 0;
    int currentRunOfSet = 0;
    List<float> bestGenCompletion = new List<float>();

    Vector3 startPos = new Vector3(-165, 85, 0);
    int columnCount = 6;
    float xSpacing = 55;
    float ySpacing = 10;
    List<Vector3> positions;

    SystemVariables SV;

    bool ended = true;

    void Start()
    {
        SV = gameObject.GetComponent<SystemVariables>();

        StartSimulation();
    }

    void StartSimulation()
    {
        carts = new List<CartBrainControl>();
        scores = new List<float>();
        ended = false;

        GenerateCombinations();
        currParamSet = paramSets[0];

        PositionSetup();
        TextActiveChange();
    }

    void PositionSetup()
    {
        positions = new List<Vector3>();
        int row = 0;
        int column = 0;
        Vector3 currentPos = startPos;
        for (int i = 0; i < currParamSet.noSimulated; i++)
        {
            positions.Add(currentPos);
            carts.Add(Instantiate(PCRObj, currentPos, Quaternion.identity).GetComponent<CartBrainControl>());
            carts[i].SetSVBC(SV, this);
            scores.Add(0);

            if (column == columnCount)
            {
                column = 0;
                row++;
                currentPos.x -= xSpacing * columnCount;
                currentPos.y -= ySpacing;
            }
            else
            {
                column++;
                currentPos.x += xSpacing;
            }
        }
    }

    void GenerateCombinations()
    {
        paramSets = new List<ParameterSet>();

        for (int i = 0; i < SV.NSAmount; i++)
        {
            paramSets.Add(new ParameterSet(0, SV.MMStart + (SV.MMInterval * Mathf.FloorToInt(SV.MMAmount/2)), 
                SV.MRStart + (SV.MRInterval * Mathf.FloorToInt(SV.MRAmount / 2)), 
                SV.MCStart + (SV.MCInterval * Mathf.FloorToInt(SV.MCAmount / 2)),
                SV.NSStart + (SV.NSInterval * i), //
                SV.PBStart + (SV.PBInterval * Mathf.FloorToInt(SV.PBAmount / 2))));
        }
        for (int i = 0; i < SV.PBAmount; i++)
        {
            paramSets.Add(new ParameterSet(1, SV.MMStart + (SV.MMInterval * Mathf.FloorToInt(SV.MMAmount / 2)), 
                SV.MRStart + (SV.MRInterval * Mathf.FloorToInt(SV.MRAmount / 2)), 
                SV.MCStart + (SV.MCInterval * Mathf.FloorToInt(SV.MCAmount / 2)), 
                SV.NSStart + (SV.NSInterval * Mathf.FloorToInt(SV.NSAmount / 2)),
                SV.PBStart + (SV.PBInterval * i))); //
        }
        for (int i = 0; i < SV.MMAmount; i++)
        {
            paramSets.Add(new ParameterSet(2, SV.MMStart + (SV.MMInterval * i), //
                SV.MRStart + (SV.MRInterval * Mathf.FloorToInt(SV.MRAmount / 2)),
                SV.MCStart + (SV.MCInterval * Mathf.FloorToInt(SV.MCAmount / 2)), 
                SV.NSStart, 
                SV.PBStart));
        }
        for (int i = 0; i < SV.MRAmount; i++)
        {
            paramSets.Add(new ParameterSet(3, SV.MMStart + (SV.MMInterval * Mathf.FloorToInt(SV.MMAmount / 2)),
                SV.MRStart + (SV.MRInterval * i), //
                SV.MCStart + (SV.MCInterval * Mathf.FloorToInt(SV.MCAmount / 2)), 
                SV.NSStart + (SV.NSInterval * Mathf.FloorToInt(SV.NSAmount / 2)), 
                SV.PBStart + (SV.PBInterval * Mathf.FloorToInt(SV.PBAmount / 2))));
        }
        for (int i = 0; i < SV.MCAmount; i++)
        {
            paramSets.Add(new ParameterSet(4, SV.MMStart + (SV.MMInterval * Mathf.FloorToInt(SV.MMAmount / 2)), 
                SV.MRStart + (SV.MRInterval * Mathf.FloorToInt(SV.MRAmount / 2)),
                SV.MCStart + (SV.MCInterval * i), //
                SV.NSStart + (SV.NSInterval * Mathf.FloorToInt(SV.NSAmount / 2)),
                SV.PBStart + (SV.PBInterval * Mathf.FloorToInt(SV.PBAmount / 2))));
        }

        //For best to be all added into
        paramSets.Add(new ParameterSet(5, SV.MMStart, SV.MRStart, SV.MCStart, SV.NSStart,SV.PBStart));

        foreach (ParameterSet ps in paramSets)
        {
            bestGenCompletion.Add(float.MaxValue);
            //ps.Print();
        }
    }

    void FixedUpdate()
    {
        if (!ended)
        {
            if (counter <= SV.genRunTime)
            {
                for (int i = 0; i < carts.Count; i++)
                {
                    //score based off of rotation
                    float rot = Mathf.Abs(carts[i].pole.transform.rotation.z);
                    if (rot <= 0.5f)
                        scores[i] += (0.5f - rot);
                    else
                        scores[i] -= (rot - 0.5f);

                    //score deduction if at the edge of the rail
                    if (carts[0].cart.transform.position.x >= carts[0].startPos.x + SV.railHalfLength
                        || carts[0].cart.transform.position.x <= carts[0].startPos.x - SV.railHalfLength)
                        scores[i] -= SV.edgeOfRailPointDeduction;


                    carts[i].scoreText.GetComponent<TextMesh>().text = "" + (((scores[i] / SV.genRunTime) / 15) * 100).ToString("F2");
                }
                counter += Time.deltaTime;
            }
            else
                StartCoroutine(End());
        }
    }

    IEnumerator End()
    {
        //end for all carts
        ended = true;
        for (int i = 0; i < carts.Count; i++)
            carts[i].End();

        //Asses scores for end condition
        for (int i = 0; i < scores.Count; i++)
            scores[i] = (((scores[i] / SV.genRunTime) / 15) * 100);
        List<float> scoresCopy = new List<float>(scores);
        scoresCopy.Sort();
        int splitPoint = Mathf.FloorToInt(scores.Count * (1 - (SV.percentileAssesed / 100)));
        bool allMeetReq = true;
        for (int i = splitPoint; i < scoresCopy.Count; i++)
        {
            if (scoresCopy[i] < SV.scoreNeeded)
            {
                allMeetReq = false;
                break;
            }
        }
        //if requirements met
        if (allMeetReq)
        {
            generationText.text = "Generation : " + generationCount + " : Requirements met";
            yield return new WaitForSeconds(0.75f);
            SetLogic();
        }
        else
        {
            //find best percentile
            splitPoint = Mathf.FloorToInt(scores.Count * (1 - (currParamSet.percentileBred / 100)));
            List<CartBrainControl> tempCarts = new List<CartBrainControl>();
            for (int i = splitPoint; i < scoresCopy.Count; i++)
            {
                carts[scores.IndexOf(scoresCopy[i])].pole.GetComponent<Pole>().SetBlue();
                if (!tempCarts.Contains(carts[scores.IndexOf(scoresCopy[i])]))
                    tempCarts.Add(carts[scores.IndexOf(scoresCopy[i])]);
            }

            yield return new WaitForSeconds(0.75f);

            //remove other carts
            for (int i = 0; i < carts.Count; i++)
            {
                if (!tempCarts.Contains(carts[i]))
                    Destroy(carts[i].gameObject);
            }

            //reset carts list
            carts = new List<CartBrainControl>(tempCarts);
            carts.Reverse();

            //move carts to new positions
            bool[] movementDone = new bool[carts.Count];
            Vector3[] startPositions = new Vector3[carts.Count];
            for (int i = 0; i < carts.Count; i++)
                startPositions[i] = carts[i].transform.position;
            float totalMoveTime = 2f;
            float movedTime = 0;
            while (movementDone.Contains(false))
            {
                movedTime += Time.deltaTime * totalMoveTime;
                for (int i = 0; i < carts.Count; i++)
                {
                    if (carts[i].transform.position != positions[i])
                        carts[i].transform.position = Vector3.Lerp(startPositions[i], positions[i], movedTime);
                    else
                        movementDone[i] = true;
                }
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(0.25f);
            StartNextGeneration(tempCarts.Count);
        }
    }

    void SetLogic()
    {
        currParamSet.genCompleted.Add(generationCount);
        generationCount = 1;
        currentRunOfSet++;

        if (currentRunOfSet > SV.runAmount-1)
        {
            currentRunOfSet = 0;
            currentParamSetNo++;

            float tempCompletion = currParamSet.AverageGenCompletion();
            if (tempCompletion < bestGenCompletion[currParamSet.testingFor])
            {
                bestGenCompletion[currParamSet.testingFor] = tempCompletion;
                for (int i = 0; i < paramSets.Count; i++)
                {
                    if (currParamSet.testingFor != paramSets[i].testingFor)
                    {
                        switch (currParamSet.testingFor)
                        {
                            case 0:
                                paramSets[i].noSimulated = currParamSet.noSimulated;
                                break;
                            case 1:
                                paramSets[i].percentileBred = currParamSet.percentileBred;
                                break;
                            case 2:
                                paramSets[i].maxMutations = currParamSet.maxMutations;
                                break;
                            case 3:
                                paramSets[i].mutationRange = currParamSet.mutationRange;
                                break;
                            case 4:
                                paramSets[i].mutationChance = currParamSet.mutationChance;
                                break;
                        }
                    }
                }
            }
            //Print best combination
            currParamSet = paramSets[currentParamSetNo];
            if (currentParamSetNo + 1 == paramSets.Count)
            {
                //destroy carts
                for (int i = 0; i < carts.Count; i++)
                    Destroy(carts[i].gameObject);

                // show optimum text
                SetOptimumText();
                currParamSet.Print();
                return; // to avoid starting next set
            }
        }
        StartNextSet();
    }

    void StartNextSet(){
        for (int i = 0; i < carts.Count; i++)
            Destroy(carts[i].gameObject);

        carts = new List<CartBrainControl>();
        scores = new List<float>();
        if (positions.Count == currParamSet.noSimulated)
        {
            for (int i = 0; i < currParamSet.noSimulated; i++)
            {
                carts.Add(Instantiate(PCRObj, positions[i], Quaternion.identity).GetComponent<CartBrainControl>());
                carts[i].SetSVBC(SV, this);
                scores.Add(0);
            }
        }
        else
            PositionSetup();

        SetMutParameterText();
        SetSetText();
        generationText.text = "Generation : " + generationCount;
        ended = false;
        counter = 0;
    }

    void StartNextGeneration(int tempCount)
    {
        //Reset
        for (int i = 0; i < scores.Count; i++)
            scores[i] = 0;
        for (int i = 0; i < carts.Count; i++)
            carts[i].Reset();

        //Crossover
        foreach (CartBrainControl c in carts)
            c.CalculateGeneticCode();

        //Place new carts
        for (int i = carts.Count; i < currParamSet.noSimulated; i++)
        {
            carts.Add(Instantiate(PCRObj, positions[i], Quaternion.identity).GetComponent<CartBrainControl>());
            carts[i].SetSVBC(SV, this);
            carts[i].geneticCode = Functions.Crossover(
                carts[UnityEngine.Random.Range(0, tempCount)].geneticCode, carts[UnityEngine.Random.Range(0, tempCount)].geneticCode);
        }

        generationCount++;
        generationText.text = "Generation: " + generationCount;
        ended = false;
        counter = 0;
    }

    void TextActiveChange()
    {
        generationText.gameObject.SetActive(true);
        sysParameterText.transform.parent.gameObject.SetActive(true);
        exitConditionText.transform.parent.gameObject.SetActive(true);
        mutParameterText.transform.parent.gameObject.SetActive(true);
        SetSysParameterText();
        SetMutParameterText();
        SetSetText();
    }

    void SetSysParameterText()
    {
        sysParameterText.text = "Pole Velocity Clamp: " + SV.poleVelocityClamp + "\nPole Initial Velocity Range: " + SV.poleInitialVelocityRange
            + "\nMax Velocity Output: " + SV.cartMaxVelocity + "\nRun Amount: " + SV.runAmount + "\nGeneration Run Time: " + SV.genRunTime;

        exitConditionText.text = "Percentile Assesed: " + SV.percentileAssesed + "\nScore Needed: " + SV.scoreNeeded;
    }
    void SetMutParameterText()
    {
        mutParameterText.gameObject.SetActive(true);

        mutParameterText.text = "0 # Simulated: " + currParamSet.noSimulated + "\n1 Percentile Bred: " + currParamSet.percentileBred 
            + "\n2 Max Mutations: " + currParamSet.maxMutations + "\n3 Mutation Range: " + currParamSet.mutationRange
            + "\n4 Mutation Chance: " + currParamSet.mutationChance;
    }

    void SetSetText()
    {
        setText.text = "Run: " + (currentRunOfSet+1) + "\nTesting For: " + currParamSet.testingFor 
            + "\nLowest Set Ave Reqs Met: " + (bestGenCompletion[currParamSet.testingFor] < float.MaxValue ? bestGenCompletion[currParamSet.testingFor].ToString() : "Not Calculated");
    }

    void SetOptimumText()
    {
        optimumText.transform.parent.gameObject.SetActive(true);
        optimumText.text = mutParameterText.text = "# Simulated: " + currParamSet.noSimulated + "\nPercentile Bred: " + currParamSet.percentileBred 
            + "\nMax Mutations: " + currParamSet.maxMutations + "\nMutation Range: " + currParamSet.mutationRange 
            + "\nMutation Chance: " + currParamSet.mutationChance + "\n\nAverage Generations: " + bestGenCompletion[currParamSet.testingFor-1];
    }
}