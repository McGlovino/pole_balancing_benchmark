﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CartBrainControl : MonoBehaviour
{
    List<layer> layers;

    public GameObject pole;
    Rigidbody2D poleRB;
    public GameObject cart;
    Rigidbody2D cartRB;
    public GameObject scoreText;

    public SystemVariables SV;
    public BenchmarkController BC;

    [HideInInspector]
    public Vector3 startPos;
    Vector2 lastVelocity;

    bool ended = false;

    [HideInInspector]
    public byte[] geneticCode;

    void Start()
    {
        cartRB = cart.GetComponent<Rigidbody2D>();
        poleRB = pole.GetComponent<Rigidbody2D>();

        startPos = cart.transform.localPosition;

        layers = new List<layer>();
        layers.Add(new layer(4));
        //layers.Add(new layer(6, layers[layers.Count-1]));
        layers.Add(new layer(4, layers[layers.Count - 1]));
        layers.Add(new layer(1, layers[layers.Count - 1]));

        if (geneticCode.Length > 0)
            ApplyGeneticCode();
    }

    void FixedUpdate()
    {
        if (!ended) {
            ///Inputs
            //Cart Position
            layers[0].nodes[0].output = Functions.RangeChange(
                cart.transform.localPosition.x,
                startPos.x - SV.railHalfLength, startPos.x + SV.railHalfLength,
                -1.0f, 1.0f);
            //Cart Velocity
            layers[0].nodes[1].output = Functions.RangeChange(
                lastVelocity.x,
                -SV.cartMaxVelocity, SV.cartMaxVelocity,
                -1.0f, 1.0f);
            //Pole Rotation
            layers[0].nodes[2].output = pole.transform.rotation.z; /* Constants.RangeChange(
           pole.transform.rotation.z,
           -180, 180,
           -1.0f, 1.0f);*/
            //Pole Velocity
            layers[0].nodes[3].output = Functions.RangeChange(
                poleRB.angularVelocity,
                -SV.poleVelocityClamp, SV.poleVelocityClamp,
                -1.0f, 1.0f);

            for (int i = 1; i < layers.Count; i++)
                layers[i].CalculateOutputs();


            float calculatedVelocity =
                Functions.RangeChange(layers[layers.Count - 1].nodes[0].output,
                0, 1, -SV.cartMaxVelocity, SV.cartMaxVelocity);

            float railClamp = 0;
            if (calculatedVelocity < 0)
                railClamp = Mathf.Clamp(cart.transform.localPosition.x - (startPos.x + SV.railHalfLength), -1, 1);
            else
                railClamp = -Mathf.Clamp(cart.transform.localPosition.x - (startPos.x - SV.railHalfLength), -1, 1);
            lastVelocity = new Vector2(railClamp * calculatedVelocity, 0);
            cartRB.velocity = lastVelocity;
        }
    }

    public void SetSVBC(SystemVariables SVT, BenchmarkController BCT)
    {
        SV = SVT;
        pole.GetComponent<Pole>().SV = SVT;
        BC = BCT;
    }

    public void End()
    {
        ended = true;
        cartRB.velocity = new Vector2(0, 0);
        cartRB.angularVelocity = 0;
        cartRB.simulated = false;
        poleRB.velocity = new Vector2(0, 0);
        poleRB.angularVelocity = 0;
        poleRB.simulated = false;
    }

    public void Reset()
    {
        cart.transform.localPosition = startPos;
        startPos = cart.transform.localPosition;
        pole.GetComponent<Pole>().Reset();

        ended = false;
        cartRB.simulated = true;
        poleRB.simulated = true;

    }

    public void CalculateGeneticCode()
    {
        if (geneticCode.Length == 0)
        {
            List<float> floatCode = new List<float>();
            foreach (layer l in layers) {
                for (int i = 0; i < l.nodes.Count; i++)
                    floatCode.AddRange(l.nodes[i].weightsIn);
            }
            geneticCode = Functions.SerializeToBytes(floatCode);
        }
    }

    public void ApplyGeneticCode()
    {
        List<float> floatCode = (List<float>)Functions.DeserializeFromBytes(geneticCode);
        Mutate(ref floatCode);

        int marker = 0;
        foreach (layer l in layers)
        {
            foreach (node n in l.nodes)
            {
                for (int i = 0; i < n.weightsIn.Count; i++)
                {
                    n.weightsIn[i] = floatCode[marker];
                    marker++;
                }
            }
        }
    }

    public void Mutate(ref List<float> floatCodeT)
    {
        for (int i = 0; i < BC.currParamSet.mutationChance; i++)
        {
            if (Random.Range(0f, 1f) <= BC.currParamSet.mutationChance /100)
            {
                int pos = Random.Range(0, floatCodeT.Count);
                floatCodeT[pos] += Random.Range(-BC.currParamSet.mutationRange, BC.currParamSet.mutationRange);
            }
        }
    }

    public class node
    {
        public int nodeNumber;
        public layer layerIn;
        public List<float> weightsIn;
        public float output;

        //input node
        public node(int nodeNumberT)
        {
            output = 0;
            weightsIn = new List<float>();

            nodeNumber = nodeNumberT;
        }
        //hidden node
        public node(int nodeNumberT, layer layerInT)
        {
            output = 0;
            nodeNumber = nodeNumberT;
            layerIn = layerInT;
            weightsIn = new List<float>();
            for (int i = 0; i < layerInT.size; i++)
                weightsIn.Add(Random.Range(-SystemVariables.Instance.weightLimit, SystemVariables.Instance.weightLimit));
        }
        public void CalculateOutput()
        {
            float dot = 0;
            for (int i = 0; i < layerIn.nodes.Count; i++)
                dot += layerIn.nodes[i].output * weightsIn[i];
            output = (1 / (1 + Mathf.Exp(-dot + SystemVariables.Instance.bias)));
        }
    }

    public class layer
    {
        //public int layerNumber;
        public int size;
        public List<node> nodes;

        //input layer
        public layer(int sizeT)
        {
            size = sizeT;
            nodes = new List<node>();
            for (int i = 0; i < size; i++)
                nodes.Add(new node(i));
        }
        //Hidden layers
        public layer(int sizeT, layer prevLayer)
        {;
            size = sizeT;
            nodes = new List<node>();
            for (int i = 0; i < size; i++)
                nodes.Add(new node(i, prevLayer));
        }
        public void CalculateOutputs()
        {
            foreach (node n in nodes)
                n.CalculateOutput();
        }
    }
}
