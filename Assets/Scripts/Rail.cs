﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rail : MonoBehaviour
{
    void Start()
    {
        transform.localScale = 
            new Vector3(SystemVariables.Instance.railHalfLength / 2, transform.localScale.z, transform.localScale.z);
    }
}
