﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroll : MonoBehaviour
{
    float startY;

    private void Start()
    {
        startY = transform.position.y;
    }
    void Update()
    {
        float newY = transform.position.y + (Input.mouseScrollDelta.y * 2);
        transform.position = new Vector3(transform.position.x, (newY <= startY ? newY : startY), transform.position.z);
    }
}
