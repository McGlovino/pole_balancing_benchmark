﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SystemVariables : MonoBehaviour
{
    private static SystemVariables SVars;
    public static SystemVariables Instance
    {
        get
        {
            if (SVars == null)
                SVars = new SystemVariables();
            return SVars;
        }
        set
        {
            SVars = value;
        }
    }

    [Header("Static Variables")]
    [HideInInspector]
    public float bias = 0;
    [HideInInspector]
    public int weightLimit = 10;
    [HideInInspector]
    public float edgeOfRailPointDeduction = 0.2f;
    [HideInInspector]
    public int railHalfLength = 20;

    public int poleVelocityClamp;
    public float cartMaxVelocity;
    public int poleInitialVelocityRange;
    public float genRunTime;
    public float runAmount;

    [Header("End Condition")]
    [Space(10)]
    public float percentileAssesed;
    public float scoreNeeded;

    [Header("# Simulated")] //order of these top headers is wierd bc unity 
    [Header("Comparible Variables")]
    [Space(10)]
    //public int noSimulated = 350;
    public int NSStart;
    public int NSInterval;
    public int NSAmount;
    [Header("Percentile Bred")]
    [Space(5)]
    //public float percentileBred = 25f;
    public int PBStart;
    public int PBInterval;
    public int PBAmount;
    [Header("Max Mutations")]
    [Space(5)]
    //public int maxMutations = 5;
    public int MMStart;
    public int MMInterval;
    public int MMAmount;
    [Header("Mutation Range")]
    [Space(5)]
    //public int mutationRange = 3;
    public int MRStart;
    public int MRInterval;
    public int MRAmount;
    [Header("Mutation Chance")]
    [Space(5)]
    //public float mutationChance = 50;
    public int MCStart;
    public int MCInterval;
    public int MCAmount;


    private void Start()
    {
        Instance = this;
    }
}
